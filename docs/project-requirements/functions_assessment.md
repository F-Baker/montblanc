# Market research

## Example sites
- discord
- zoom
- google meets
- jitsi
- https://moodle.org/
- https://nextcloud.com/ <---- this seems to be the best
- ent


### Oddities
- https://stormboard.com/


## Project features
### Primary
- Course
    - face book
    
- accounts
    - attendance
    - participation
    - grades
    - roles
    - messaging ?
    - signup
    - description
    
- Kanban/calendar 
- search




### Secondary
- Channels
    - poling
    - quizzes
    - tests
    - chat

    
# Questions about questionnaires, questions, evaluations, and other types of quizzes
## What elements does each have?

### Question
- content
- type
- reactions
- response
- create
- update
#### Requires
- course id to correspond with the subject
- prof ID to create/edit
- stu ID to respond/react
- partipation id which serves as a list to record




# Design Decisions
- no birthdays
    - staff doesn't require it and it's a simple addition for the students
    - never used
    
- "session de formation" = "Course"
